import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import { Quote } from '../../data/quotes.interface';
import quotes from '../../data/quotes';

/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage {

  quoteCollection: {category:string, quotes:Quote[], icon:string}[];
  

  goToDetails(item) {
    this.navCtrl.push("QuotesPage",item);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    
  }

  ngOnInit() {
    this.quoteCollection = quotes;
    console.log(this.quoteCollection);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LibraryPage');
  }

}
