import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { Quote } from '../../data/quotes.interface';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams,  private quotesService: QuotesService, private modalCtrl: ModalController) {
  }
  quoteCollection: Quote[];

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
    
  }
  ionViewWillEnter() {
    this.quoteCollection = this.quotesService.getAllFavoriteQuotes();
    console.log(this.quoteCollection);
  }

  ngOnInit() {

  }

  removeQuotes(item) {
  this.quotesService.removeQuoteFromFavorites(item);
  }

  openModal(item) {
    let modal = this.modalCtrl.create("FavoritesmodalPage", item);
    modal.present();
  }

}
