import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  logEvent(event) {
    console.log(event);
  }

  goToPage1() {
    this.navCtrl.push("FirstPage",{nama:'Johannes',umur:'20'});
  }


  constructor(public navCtrl: NavController) {

  }

}
