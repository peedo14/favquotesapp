import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { Quote } from '../../data/quotes.interface';

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage {

  quoteCollection: Quote[];

  public result : number = null;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,  private quotesService: QuotesService, public alertCtrl: AlertController) {
  }

  ionViewWillEnter() {
    var item = this.navParams.data;
    this.quoteCollection = item;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotesPage');
    var item = this.navParams.data;
    this.quoteCollection = item;
  }


  isFav(quote) {
    const result = this.quotesService.isFavorite(quote);
    return result;
  }

  unFav(quote) {
    const result = this.quotesService.isNotFavorite(quote);
    return result;
  }

  onAddQuote(quote: Quote) {
      const confirm = this.alertCtrl.create({
        title: 'add Quote',
        message: 'Do you agree to add '+quote.person+' to your Favorites?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              this.quotesService.addQuoteToFavorites(quote);
            }
          }
        ]
      });
      confirm.present();
    } 

    onDeleteQuote(quote: Quote) {
      const confirm = this.alertCtrl.create({
        title: 'Remove Quote',
        message: 'Do you agree to remove '+quote.person+' to your Favorites?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              this.quotesService.removeQuoteFromFavorites(quote);
            }
          }
        ]
      });
      confirm.present();
    } 
}
