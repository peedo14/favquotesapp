import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritesmodalPage } from './favoritesmodal';

@NgModule({
  declarations: [
    FavoritesmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritesmodalPage),
  ],
})
export class FavoritesmodalPageModule {}
