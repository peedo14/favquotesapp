import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { Quote } from '../../data/quotes.interface';

/**
 * Generated class for the FavoritesmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoritesmodal',
  templateUrl: 'favoritesmodal.html',
})
export class FavoritesmodalPage {


  constructor(public navCtrl: NavController, public navParams: NavParams,  private quotesService: QuotesService) {
  }
  quoteCollection: any = [];

  ionViewDidLoad() {
    var result = this.quotesService.getFavoriteQuotes(this.navParams.data);
    this.quoteCollection = result;
  }

  closeModal() {
    this.navCtrl.pop();
  }

  onDeleteQuote(quote: Quote) {
            this.quotesService.removeQuoteFromFavorites(quote);
            this.navCtrl.pop();
  }
}
