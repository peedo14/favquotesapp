import { Quote } from '../data/quotes.interface';

export class QuotesService {

public favoriteQuotes: Quote[] = [];

addQuoteToFavorites(quote: Quote) {
this.favoriteQuotes.push(quote);
}

removeQuoteFromFavorites(quote: Quote) {
    var jumlahQuotes = this.favoriteQuotes.length;
    var i = 0;
    while(jumlahQuotes > i) {
      if (quote.id == this.favoriteQuotes[i].id) {
          this.favoriteQuotes.splice(i,1);
        break;
      }
      i++;
    }
}
getFavoriteQuotes(quote) {
    var jumlahQuotes = this.favoriteQuotes.length;
    var i = 0;
    while(jumlahQuotes > i) {
      if (quote.id == this.favoriteQuotes[i].id) {
        return this.favoriteQuotes[i];
      }
      i++;
    }
}
isFavorite(quote) {

    var jumlahQuotes = this.favoriteQuotes.length;
    var isFavorite;
    var i = 0;
    if(jumlahQuotes == i) {
        isFavorite = 1;
    }
    while(jumlahQuotes > i) {
    if (quote == this.favoriteQuotes[i].id) {
        isFavorite = 0;
    break;
    } else {
    isFavorite = 1;
    }
    i++;
    }
    return isFavorite;
}

isNotFavorite(quote) {

    var jumlahQuotes = this.favoriteQuotes.length;
    var isNotFavorite;
    var i = 0;
    if(jumlahQuotes == i) {

        isNotFavorite = 0;
    }
    while(jumlahQuotes > i) {

    if (quote == this.favoriteQuotes[i].id) {

        isNotFavorite = 1;
    break;
    } else {

    isNotFavorite = 0;
    }
    i++;
    }

    return isNotFavorite;
}

getAllFavoriteQuotes(){
    return this.favoriteQuotes;
}
}